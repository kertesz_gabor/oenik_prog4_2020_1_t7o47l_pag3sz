﻿// <copyright file="IDifficultyPreset.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    /// <summary>
    /// Defines the structure of a difficulty implementation.
    /// </summary>
    public interface IDifficultyPreset
    {
        /// <summary>
        /// Gets or sets the number of cops associatiated with the difficulty preset.
        /// </summary>
        int NumberOfCops { get; set; }

        /// <summary>
        /// Gets or sets the name of the preset.
        /// </summary>
        string PresetName { get; set; }
    }

    /// <summary>
    /// Represents a difficulty preset.
    /// </summary>
    public class DifficultyPreset : IDifficultyPreset
    {
        /// <inheritdoc/>
        public int NumberOfCops { get; set; }

        /// <inheritdoc/>
        public string PresetName { get; set; }
    }
}