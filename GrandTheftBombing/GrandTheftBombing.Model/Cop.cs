﻿// <copyright file="Cop.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    /// <summary>
    /// An <see cref="ICop"/> implementation.
    /// </summary>
    public class Cop : ICop
    {
        private Direction direction;
        private double x;
        private double y;
        private double dx;
        private double dy;

        /// <inheritdoc/>
        public Direction Direction
        {
            get { return this.direction; } set { this.direction = value; }
        }

        /// <inheritdoc/>
        public double X
        {
            get { return this.x; } set { this.x = value; }
        }

        /// <inheritdoc/>
        public double Y
        {
            get { return this.y; } set { this.y = value; }
        }

        /// <inheritdoc/>
        public double DX
        {
            get { return this.dx; } set { this.dx = value; }
        }

        /// <inheritdoc/>
        public double DY
        {
            get { return this.dy; } set { this.dy = value; }
        }

        /// <inheritdoc/>
        public int StuckCooldown { get; set; }

        /// <inheritdoc/>
        public int AttemptsToMove { get; set; }
    }
}
