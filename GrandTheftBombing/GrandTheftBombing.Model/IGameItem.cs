﻿// <copyright file="IGameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    /// <summary>
    /// Defines the components needed to represent a game item, such as bullets and bombs.
    /// </summary>
    public interface IGameItem
    {
        /// <summary>
        /// Gets or sets the position of the item on the x-axis.
        /// </summary>
        int X { get; set; }

        /// <summary>
        /// Gets or sets the position of the item on the y-axis.
        /// </summary>
        int Y { get; set; }

        /// <summary>
        /// Sets the position of the game item.
        /// </summary>
        /// <param name="x">The x component to set.</param>
        /// <param name="y">The y component to set.</param>
        void SetPosition(int x, int y);
    }
}
