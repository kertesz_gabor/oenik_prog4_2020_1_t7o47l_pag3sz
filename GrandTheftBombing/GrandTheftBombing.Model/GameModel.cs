﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    using System.Collections.Generic;
    using System.Drawing;

    /// <summary>
    /// An <see cref="IGameModel"/> implementation.
    /// </summary>
    public class GameModel : IGameModel
    {
        private List<Point> mapentrypoints;
        private List<Cop> cops;
        private bool[,] roads;
        private Player player;
        private double width;
        private double height;
        private double tilesize;
        private List<Bullet> bullets;
        private List<Point> walls;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="w">The width to set.</param>
        /// <param name="h">The height to set.</param>
        public GameModel(double w, double h)
        {
            this.Walls = new List<Point>();
            this.Cops = new List<Cop>();
            this.Bullets = new List<Bullet>();
            this.TileSize = 25;

            this.GameWidth = w;
            this.GameHeight = h;

            this.GameWidth = w;
            this.GameHeight = h;
        }

        /// <inheritdoc/>
        public List<Point> MapEntryPoints
        {
            get { return this.mapentrypoints; } set { this.mapentrypoints = value; }
        }

        /// <inheritdoc/>
        public List<Cop> Cops
        {
            get { return this.cops; } set { this.cops = value; }
        }

        /// <inheritdoc/>
        public bool[,] Roads
        {
            get { return this.roads; } set { this.roads = value; }
        }

        /// <inheritdoc/>
        public Player MyPlayer
        {
            get { return this.player; } set { this.player = value; }
        }

        /// <inheritdoc/>
        public double GameWidth
        {
            get { return this.width; } set { this.width = value; }
        }

        /// <inheritdoc/>
        public double GameHeight
        {
            get { return this.height; } set { this.height = value; }
        }

        /// <inheritdoc/>
        public double TileSize
        {
            get { return this.tilesize; } set { this.tilesize = value; }
        }

        /// <inheritdoc/>
        public IDifficultyPreset SelectedDifficulty { get; set; }

        /// <inheritdoc/>
        public List<Bullet> Bullets
        {
            get { return this.bullets; } set { this.bullets = value; }
        }

        /// <inheritdoc/>
        public List<Point> Walls
        {
            get { return this.walls; } set { this.walls = value; }
        }
    }
}
