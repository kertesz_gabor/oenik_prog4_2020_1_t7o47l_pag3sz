﻿// <copyright file="Bullet.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    /// <summary>
    /// An <see cref="IMovable"/> bullet implementation.
    /// </summary>
    public class Bullet : IMovable
    {
        private Direction direction;
        private double x;
        private double y;
        private double dx;
        private double dy;
        private bool delete;

        /// <summary>
        /// Initializes a new instance of the <see cref="Bullet"/> class.
        /// </summary>
        /// <param name="x">The x component of the coordinate of the object.</param>
        /// <param name="y">The y component of coordinate of the object.</param>
        /// <param name="speedX">The x component of the speed the object is moving at.</param>
        /// <param name="speedY">The y component of the speed the object is moving at.</param>
        public Bullet(double x, double y, double speedX, double speedY)
        {
            this.x = x;
            this.y = y;
            this.dx = speedX;
            this.dy = speedY;
        }

        /// <inheritdoc/>
        public Direction Direction
        {
            get { return this.direction; } set { this.direction = value; }
        }

        /// <inheritdoc/>
        public double X
        {
            get { return this.x; } set { this.x = value; }
        }

        /// <inheritdoc/>
        public double Y
        {
            get { return this.y; } set { this.y = value; }
        }

        /// <inheritdoc/>
        public double DX
        {
            get { return this.dx; } set { this.dx = value; }
        }

        /// <inheritdoc/>
        public double DY
        {
            get { return this.dy; } set { this.dy = value; }
        }

        /// <inheritdoc/>
        public bool Delete
        {
            get { return this.delete; } set { this.delete = value; }
        }

        /// <inheritdoc/>
        public void ChangeCoordinatesBySpeed()
        {
            this.x += this.DX;
            this.y += this.DY;
        }
    }
}
