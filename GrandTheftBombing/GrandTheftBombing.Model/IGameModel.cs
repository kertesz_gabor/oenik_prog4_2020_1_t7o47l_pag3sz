﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    using System.Collections.Generic;
    using System.Drawing;

    /// <summary>
    /// Declares which components are needed to represent the game's state.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets the selected difficulty.
        /// </summary>
        IDifficultyPreset SelectedDifficulty { get; set; }

        /// <summary>
        /// Gets or sets the list containing the bullets in the game.
        /// </summary>
        List<Bullet> Bullets { get; set; }

        /// <summary>
        /// Gets or sets the walls on the gamemap.
        /// </summary>
        List<Point> Walls { get; set; }

        /// <summary>
        /// Gets or sets the coordinates of the points where entry is possible for cops.
        /// </summary>
        List<Point> MapEntryPoints { get; set; }

        /// <summary>
        /// Gets or sets cop instances in the game.
        /// </summary>
        List<Cop> Cops { get; set; }

        /// <summary>
        /// Gets or sets the matrix representing the map of the game.
        /// </summary>
        bool[,] Roads { get; set; }

        /// <summary>
        /// Gets or sets the only player instance of the game.
        /// </summary>
        Player MyPlayer { get; set; }

        /// <summary>
        /// Gets or sets the width of the current game session.
        /// </summary>
        double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets the height of the current game session.
        /// </summary>
        double GameHeight { get; set; }

        /// <summary>
        /// Gets or sets the definition of one tile in the game.
        /// </summary>
        double TileSize { get; set; }
    }
}
