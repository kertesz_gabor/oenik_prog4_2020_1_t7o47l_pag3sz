﻿// <copyright file="Direction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    /// <summary>
    /// Represents the facing direction of the in-game elements.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Direction facing upwards.
        /// </summary>
        Up,

        /// <summary>
        /// Direction facing right.
        /// </summary>
        Right,

        /// <summary>
        /// Direction facing downwards.
        /// </summary>
        Down,

        /// <summary>
        /// Direction facing left.
        /// </summary>
        Left,
    }
}
