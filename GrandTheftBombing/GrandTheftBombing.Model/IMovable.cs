﻿// <copyright file="IMovable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    /// <summary>
    /// Provides a layout for how a movable object should behave in the game.
    /// </summary>
    public interface IMovable
    {
        /// <summary>
        /// Gets or sets the direction to move the object in.
        /// </summary>
        Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the coordinate of the object on the x-axis.
        /// </summary>
        double X { get; set; }

        /// <summary>
        /// Gets or sets the coordinate of the object on the y-axis.
        /// </summary>
        double Y { get; set; }

        /// <summary>
        /// Gets or sets the x component of the speed vector.
        /// </summary>
        double DX { get; set; }

        /// <summary>
        /// Gets or sets the x component of the speed vector.
        /// </summary>
        double DY { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the the object is deleted or not.
        /// </summary>
        bool Delete { get; set; }

        /// <summary>
        /// Changes the object position by the speed vector.
        /// </summary>
        void ChangeCoordinatesBySpeed();
    }
}
