﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    using System.Drawing;

    /// <summary>
    /// Defines a player as a special cop.
    /// </summary>
    public class Player : Cop
    {
        /// <summary>
        /// Gets or sets the name of the player.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the number of kills the player has.
        /// </summary>
        public int Kills { get; set; }

        /// <summary>
        /// Gets or sets the number of lives a player has.
        /// </summary>
        public double Lives { get; set; }

        /// <summary>
        /// Gets or sets the position of the player.
        /// </summary>
        public Point PlayerPos { get; set; }
    }
}
