﻿// <copyright file="ICop.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Model
{
    /// <summary>
    /// Defines a cop component in the game.
    /// </summary>
    public interface ICop
    {
        /// <summary>
        /// Gets or sets the direction the cop instance is facing.
        /// </summary>
        Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets the x component of the cop's position.
        /// </summary>
        double X { get; set; }

        /// <summary>
        /// Gets or sets the y component of the cop's position.
        /// </summary>
        double Y { get; set; }

        /// <summary>
        /// Gets or sets the cop's speed on the horizontal axis.
        /// </summary>
        double DX { get; set; }

        /// <summary>
        /// Gets or sets the cop's speed on the horizontal axis.
        /// </summary>
        double DY { get; set; }

        /// <summary>
        /// Gets or sets the cooldown number. If the cop stuck, the logic denies it moving until the cooldown is up.
        /// </summary>
        int StuckCooldown { get; set; }

        /// <summary>
        /// Gets or sets how many times the cop tried to move.
        /// </summary>
        int AttemptsToMove { get; set; }
    }
}
