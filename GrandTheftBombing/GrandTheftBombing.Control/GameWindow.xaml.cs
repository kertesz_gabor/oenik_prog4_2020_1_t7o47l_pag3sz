﻿// <copyright file="GameWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Control
{
    using System.Windows;
    using GrandTheftBombing.Model;

    /// <summary>
    /// Interaction logic for MenuWindow.xaml.
    /// </summary>
    public partial class GameWindow : Window
    {
        private IDifficultyPreset difficultyPreset;
        private string playerName;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        /// <param name="chosenDifficultyPreset">The difficulty preset to start the game with.</param>
        /// <param name="playerName">The player name to start the game with.</param>
        public GameWindow(IDifficultyPreset chosenDifficultyPreset, string playerName)
        {
            this.InitializeComponent();
            this.difficultyPreset = chosenDifficultyPreset;
            this.playerName = playerName;
        }

        private void HighscoreClick(object sender, RoutedEventArgs e)
        {
            HighscoreWindow hw = new HighscoreWindow();
            hw.Show();
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BackToMenuClick(object sender, RoutedEventArgs e)
        {
            MainWindow.GetWindow(this).Activate();
            this.Close();
        }

        private void Window_ContentRendered(object sender, System.EventArgs e)
        {
            (this.display as GameControl).Model.SelectedDifficulty = this.difficultyPreset;
            (this.display as GameControl).Logic.Model = (this.display as GameControl).Model;
            (this.display as GameControl).Logic.Model.MyPlayer.Name = this.playerName;
        }
    }
}
