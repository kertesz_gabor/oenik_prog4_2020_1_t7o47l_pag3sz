﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Control
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using GrandTheftBombing.Logic;
    using GrandTheftBombing.Model;
    using GrandTheftBombing.Renderer;
    using GrandTheftBombing.Repository;

    /// <summary>
    /// Provides funcionality for controlling the game.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private ILogic logic;
        private GameRenderer renderer;
        private IGameModel model;
        private IRepository repository;
        private Stopwatch stw;
        private Window win;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            this.Loaded += this.LoadControl;
        }

        /// <summary>
        /// Gets or sets the current gamemodel.
        /// </summary>
        public IGameModel Model
        {
            get { return this.model; }
            set { this.model = value; }
        }

        /// <summary>
        /// Gets or sets the current gamelogic.
        /// </summary>
        public ILogic Logic { get => this.logic; set => this.logic = value; }

        private DispatcherTimer BulletMoverDt { get; set; }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.Logic != null)
            {
                if (!this.Logic.HasThePlayerLost() && this.model.Cops.Count > 0)
                {
                    if (this.renderer != null)
                    {
                        drawingContext.DrawDrawing(this.renderer.BuildDrawing(this.Logic.MoveCop));
                    }
                }
                else
                {
                    this.BulletMoverDt.Stop();
                    ResultWindow resultWindow = new ResultWindow(this.model.MyPlayer.Kills);
                    this.win.Close();
                    this.Logic.SaveGame();
                    resultWindow.Show();
                }
            }
        }

        private void LoadControl(object sender, RoutedEventArgs e)
        {
            this.stw = new Stopwatch();
            this.model = new GameModel(800, 800);
            this.repository = new GameRepository(this.model);
            this.Logic = new GameLogic(this.model, this.repository);
            App.Logic = this.Logic;
            this.renderer = new GameRenderer(this.model);

            this.BulletMoverDt = new DispatcherTimer();
            this.BulletMoverDt.Interval = TimeSpan.FromMilliseconds(40);
            this.BulletMoverDt.Tick += (x, y) =>
            {
                var bulletsToDelete = new List<Bullet>();
                foreach (var a in this.model.Bullets)
                {
                    a.ChangeCoordinatesBySpeed();
                    this.Logic.DoesBulletReachWallOrCop(a);
                }

                this.model.Bullets.RemoveAll(b => b.Delete);

                this.InvalidateVisual();
            };

            this.win = Window.GetWindow(this);
            if (this.win != null)
            {
                this.win.KeyDown += this.Win_KeyDown;
                this.BulletMoverDt.Start();
            }

            this.InvalidateVisual();
            this.stw.Start();
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            bool finished = false;

            switch (e.Key)
            {
                case Key.W: finished = this.Logic.MovePlayer(Direction.Up); break;
                case Key.S: finished = this.Logic.MovePlayer(Direction.Down); break;
                case Key.D: finished = this.Logic.MovePlayer(Direction.Right); break;
                case Key.A: finished = this.Logic.MovePlayer(Direction.Left); break;
                case Key.F: this.Logic.Shoot(); break;
            }

            this.InvalidateVisual();
        }
    }
}
