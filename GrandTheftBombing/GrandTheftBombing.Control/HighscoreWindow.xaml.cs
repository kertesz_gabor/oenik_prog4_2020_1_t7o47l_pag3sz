﻿// <copyright file="HighscoreWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Control
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for HighscoreWindow.xaml.
    /// </summary>
    public partial class HighscoreWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreWindow"/> class.
        /// </summary>
        public HighscoreWindow()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
