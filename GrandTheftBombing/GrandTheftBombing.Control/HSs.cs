﻿// <copyright file="HSs.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Control
{
    using System.Collections.Generic;
    using GrandTheftBombing.Repository;

    /// <summary>
    /// Stores the highscore records in the control.
    /// </summary>
    public class HSs : List<HighscoreRecord>
    {
        private GameRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="HSs"/> class.
        /// </summary>
        public HSs()
        {
            this.repo = new GameRepository();
            this.AddRange(this.repo.Highscores);
        }
    }
}