﻿// <copyright file="ResultWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Control
{
    using System.Windows;

    /// <summary>
    /// Interaction logic for ResultWindow.xaml.
    /// </summary>
    public partial class ResultWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ResultWindow"/> class.
        /// </summary>
        /// <param name="kills">The number of kills to display.</param>
        public ResultWindow(int kills)
        {
            this.InitializeComponent();
            this.lbl_result.Content = $"Your score is: {kills}";
        }
    }
}
