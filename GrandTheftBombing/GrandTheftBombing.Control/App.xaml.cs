﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Control
{
    using System.Windows;
    using GrandTheftBombing.Logic;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Gets or sets the concrete implementation of the <see cref="ILogic"/> interface.
        /// </summary>
        public static ILogic Logic { get; set; }
    }
}
