﻿// <copyright file="DifficultyChooser.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Control
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using GrandTheftBombing.Model;

    /// <summary>
    /// Interaction logic for DifficultyChooser.xaml.
    /// </summary>
    public partial class DifficultyChooser : Window
    {
        private IDifficultyPreset chosenPreset;
        private List<IDifficultyPreset> difficulties;

        /// <summary>
        /// Initializes a new instance of the <see cref="DifficultyChooser"/> class.
        /// </summary>
        /// <param name="currentPreset">The currently set preset.</param>
        /// <param name="difficulties">The list of difficulties to choose from.</param>
        public DifficultyChooser(IDifficultyPreset currentPreset, List<IDifficultyPreset> difficulties)
        {
            this.InitializeComponent();
            this.ChosenPreset = currentPreset;
            this.difficulties = difficulties;
            this.RenderOptions();
        }

        /// <summary>
        /// Gets or sets the selected difficulty.
        /// </summary>
        public IDifficultyPreset ChosenPreset { get => this.chosenPreset; set => this.chosenPreset = value; }

        private void RenderOptions()
        {
            string presetName = this.ChosenPreset.PresetName;
            switch (presetName)
            {
                case "Easy": this.r_easy.IsChecked = true; break;
                case "Medium": this.r_medium.IsChecked = true; break;
                case "Hard": this.r_hard.IsChecked = true;
                    break;
            }
        }

        private void Back_click(object sender, RoutedEventArgs e)
        {
            MainWindow.GetWindow(this).Activate();
            this.Close();
        }

        private void Save_click(object sender, RoutedEventArgs e)
        {
            var checkedPreset = this.grid.Children.OfType<RadioButton>().Where(x => x.IsChecked == true).FirstOrDefault().Name;
            switch (checkedPreset)
            {
                case "r_easy": this.ChosenPreset = this.difficulties.Find(x => x.PresetName == "Easy"); break;
                case "r_medium": this.ChosenPreset = this.difficulties.Find(x => x.PresetName == "Medium"); break;
                case "r_hard": this.ChosenPreset = this.difficulties.Find(x => x.PresetName == "Hard"); break;
            }

            this.DialogResult = true;
        }
    }
}
