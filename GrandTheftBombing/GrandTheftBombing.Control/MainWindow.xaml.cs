﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Control
{
    using System.Linq;
    using System.Windows;
    using System.Windows.Input;
    using GrandTheftBombing.Model;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        private IDifficultyPreset selectedDifficulty;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.selectedDifficulty = Logic.Difficulty.Difficulties.First();
            this.DataContext = new Player() { Name = "Anonymus" };
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void HighscoreClick(object sender, RoutedEventArgs e)
        {
            HighscoreWindow hw = new HighscoreWindow();
            hw.Show();
        }

        private void ExitClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void StartGameClick(object sender, RoutedEventArgs e)
        {
            GameWindow gameWindow = new GameWindow(this.selectedDifficulty, (this.DataContext as Player).Name);
            gameWindow.Show();
        }

        private void SetDifficultyClick(object sender, RoutedEventArgs e)
        {
            DifficultyChooser difficultyChooserWin = new DifficultyChooser(this.selectedDifficulty, Logic.Difficulty.Difficulties);
            if (difficultyChooserWin.ShowDialog() == true)
            {
                this.selectedDifficulty = difficultyChooserWin.ChosenPreset;
            }
        }
    }
}
