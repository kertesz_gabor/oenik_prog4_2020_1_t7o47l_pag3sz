﻿// <copyright file="GameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Renderer
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using GrandTheftBombing.Model;

    /// <summary>
    /// Renders the game.
    /// </summary>
    public class GameRenderer
    {
        private IGameModel model;
        private List<Drawing> walls;
        private Drawing player;
        private List<Drawing> cop;
        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();
        private System.Drawing.Point pos;
        private Random rnd = new Random();

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">The <see cref="IGameModel"/> implementation to work with.</param>
        public GameRenderer(IGameModel model)
        {
            this.model = model;
            this.walls = new List<Drawing>();
        }

        private Brush PlayerBrush
        {
            get { return this.GetBrush($"GrandTheftBombing.Renderer.Images.player-{this.model.MyPlayer.Direction.ToString().ToLower()}.bmp", false); }
        }

        private Brush CopBrush
        {
            get { return this.GetBrush("GrandTheftBombing.Renderer.Images.cop.bmp", false); }
        }

        private Brush WallBrush
        {
            get { return this.GetBrush("GrandTheftBombing.Renderer.Images.wall.bmp", false); }
        }

        private Brush BulletBrush
        {
            get { return this.GetBrush("GrandTheftBombing.Renderer.Images.bullet.bmp", false); }
        }

        /// <summary>
        /// Resets the game render.
        /// </summary>
        public void Reset()
        {
            this.walls = null;
            this.player = null;
            this.cop = null;
            this.pos = new System.Drawing.Point(-1, -1);
            this.model.Cops.Clear();
            this.brushes.Clear();
        }

        /// <summary>
        /// Builds the needed drawings into one <see cref="DrawingGroup"/>.
        /// </summary>
        /// <param name="moveCop">The logical way to move a cop.</param>
        /// <returns>A <see cref="DrawingGroup"/>.</returns>
        public Drawing BuildDrawing(Action<Cop> moveCop)
        {
            DrawingGroup dg = new DrawingGroup();
            this.GetWalls().ForEach(a => dg.Children.Add(a));
            dg.Children.Add(this.GetPlayer());
            this.GetCops().ForEach(a => dg.Children.Add(a));
            this.GetBullets().ForEach(a => dg.Children.Add(a));

            this.model.Cops.ForEach(a => moveCop(a));
            return dg;
        }

        private List<Drawing> GetCops()
        {
            this.cop = new List<Drawing>();
            for (var i = 0; i < this.model.Cops.Count; i++)
            {
                Geometry g = new RectangleGeometry(new Rect(this.model.Cops[i].X * this.model.TileSize, this.model.Cops[i].Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                RotateTransform aRotateTransform = new RotateTransform();
                aRotateTransform.CenterX = 0.5;
                aRotateTransform.CenterY = 0.5;
                aRotateTransform.Angle = this.GetRotationAngle(this.model.Cops[i]);
                Brush copBrush = this.CopBrush;
                copBrush.RelativeTransform = aRotateTransform;

                this.cop.Add(new GeometryDrawing(copBrush, null, g));
            }

            if (this.pos != this.model.MyPlayer.PlayerPos)
            {
                Geometry g = new RectangleGeometry(new Rect(this.model.MyPlayer.X * this.model.TileSize, this.model.MyPlayer.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                this.player = new GeometryDrawing(this.PlayerBrush, null, g);
                this.pos = this.model.MyPlayer.PlayerPos;
            }

            return this.cop;
        }

        private int GetRotationAngle(Cop cop)
        {
            switch (cop.Direction)
            {
                case Direction.Up: return 270;
                case Direction.Right: return 180;
                case Direction.Down: return 90;
                case Direction.Left: return 0;
            }

            return 0;
        }

        private Drawing GetPlayer()
        {
            if (this.player == null || this.pos.X != this.model.MyPlayer.X || this.pos.Y != this.model.MyPlayer.Y)
            {
                Geometry g = new RectangleGeometry(new Rect(this.model.MyPlayer.X * this.model.TileSize, this.model.MyPlayer.Y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                this.player = new GeometryDrawing(this.PlayerBrush, null, g);
                this.pos = this.model.MyPlayer.PlayerPos;
            }

            return this.player;
        }

        private List<Drawing> GetBullets()
        {
            List<Drawing> bullets = new List<Drawing>();

            foreach (var x in this.model.Bullets)
            {
                Geometry bulletTile = new RectangleGeometry(new Rect(x.X * this.model.TileSize, x.Y * this.model.TileSize, 15, 15));
                bullets.Add(new GeometryDrawing(this.BulletBrush, null, bulletTile));
            }

            return bullets;
        }

        private List<Drawing> GetWalls()
        {
            if (this.walls == null || this.walls.Count == 0)
            {
                for (int x = 0; x < this.model.Roads.GetLength(0); x++)
                {
                    for (int y = 0; y < this.model.Roads.GetLength(1); y++)
                    {
                        if (!this.model.Roads[x, y])
                        {
                            Geometry streetTile = new RectangleGeometry(new Rect(x * this.model.TileSize, y * this.model.TileSize, this.model.TileSize, this.model.TileSize));
                            this.walls.Add(new GeometryDrawing(this.WallBrush, null, streetTile));
                        }
                    }
                }
            }

            return this.walls;
        }

        /// <summary>
        /// Creates an imagebrush from a texture.
        /// </summary>
        /// <param name="filePath">The file containing the texture.</param>
        /// <param name="isTiled">Tells whether the texture is tiled or not.</param>
        /// <returns>An imagebrush.</returns>
        private Brush GetBrush(string filePath, bool isTiled)
        {
            if (!this.brushes.ContainsKey(filePath))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream(filePath);
                bmp.EndInit();
                ImageBrush imageBrush = new ImageBrush(bmp);
                if (isTiled)
                {
                    imageBrush.TileMode = TileMode.Tile;
                    imageBrush.Viewport = new Rect(0, 0, this.model.TileSize, this.model.TileSize);
                    imageBrush.ViewportUnits = BrushMappingMode.Absolute;
                }

                this.brushes[filePath] = imageBrush;
            }

            return this.brushes[filePath];
        }
    }
}
