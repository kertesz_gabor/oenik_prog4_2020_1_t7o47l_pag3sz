var interface_grand_theft_bombing_1_1_logic_1_1_i_logic =
[
    [ "ChangeVehicleDirection", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a2eca2664a2e4c634cd4897b09a7aa2cd", null ],
    [ "DoesBulletReachWallOrCop", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a0f64073fbaf89a07455608d42946b733", null ],
    [ "GetDifficulties", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a62082d796c2f3b01ed58788448e8119c", null ],
    [ "GetDirectionToPlayer", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#af34e9874c6c2e0fa2af7161486495755", null ],
    [ "HasThePlayerLost", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#ac758162cd4df0b9ab68d57becc6d2f85", null ],
    [ "InitMap", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a65c90f30589a4e7707151cb5abc66398", null ],
    [ "IsValidStep", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#adc330e5f468e639ed34e513bb3fd9d58", null ],
    [ "LoadGame", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a12580eec9aee043a08b0e7841a50fd61", null ],
    [ "MoveCop", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a26f410221f520ba2d6b862583da0cde4", null ],
    [ "MoveCops", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a86806642af3a2cb3929cc93b6ec19746", null ],
    [ "MovePlayer", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a95b9c3150ba9bb9910f78058cddeb7e8", null ],
    [ "SaveGame", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#ad5660510807b4b091ebee2a0202f2c06", null ],
    [ "SetDifficulty", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a232c87853074082bcfc0215d6079fccd", null ],
    [ "Shoot", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#ab08b4f6605cc3b4a62314911ad553a13", null ],
    [ "Model", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#af072dc447b19795395b5bee5f31a2abd", null ],
    [ "Repository", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html#a8ed21da8c132749a8aab0d5742735297", null ]
];