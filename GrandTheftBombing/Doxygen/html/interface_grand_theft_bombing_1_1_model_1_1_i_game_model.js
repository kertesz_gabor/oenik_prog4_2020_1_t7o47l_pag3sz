var interface_grand_theft_bombing_1_1_model_1_1_i_game_model =
[
    [ "Bullets", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#a36ccd1ce8829035f0a526580701446df", null ],
    [ "Cops", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#a1d97ca96aa9235f895970febd218de7b", null ],
    [ "GameHeight", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#a6820e3f96a9226699bb5c1ce71713700", null ],
    [ "GameWidth", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#ab187556b3c01e03b3ea584436d409dd6", null ],
    [ "MapEntryPoints", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#a083ab6dd7f980377876fee51af51b002", null ],
    [ "MyPlayer", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#a4cc88fdbd0097a1ff44a0a0e7bb870bc", null ],
    [ "Roads", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#a3707d9b299d404ea341a60045a2dcf83", null ],
    [ "SelectedDifficulty", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#a79542703e0f69bd6e8d0d63fbbe35626", null ],
    [ "TileSize", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#a31e133f0e45de072bfb7e6e9ded6e65e", null ],
    [ "Walls", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html#aedd31ce40f22bc83427feb07a83771b3", null ]
];