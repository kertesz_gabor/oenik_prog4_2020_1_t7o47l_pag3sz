var namespace_grand_theft_bombing_1_1_repository =
[
    [ "GameRepository", "class_grand_theft_bombing_1_1_repository_1_1_game_repository.html", "class_grand_theft_bombing_1_1_repository_1_1_game_repository" ],
    [ "HighscoreRecord", "class_grand_theft_bombing_1_1_repository_1_1_highscore_record.html", "class_grand_theft_bombing_1_1_repository_1_1_highscore_record" ],
    [ "IHighscoreRecord", "interface_grand_theft_bombing_1_1_repository_1_1_i_highscore_record.html", "interface_grand_theft_bombing_1_1_repository_1_1_i_highscore_record" ],
    [ "IRepository", "interface_grand_theft_bombing_1_1_repository_1_1_i_repository.html", "interface_grand_theft_bombing_1_1_repository_1_1_i_repository" ]
];