var hierarchy =
[
    [ "Application", null, [
      [ "GrandTheftBombing.Control.App", "class_grand_theft_bombing_1_1_control_1_1_app.html", null ]
    ] ],
    [ "Application", null, [
      [ "GrandTheftBombing.Main.App", "class_grand_theft_bombing_1_1_main_1_1_app.html", null ]
    ] ],
    [ "GrandTheftBombing.Logic.Config", "class_grand_theft_bombing_1_1_logic_1_1_config.html", null ],
    [ "FrameworkElement", null, [
      [ "GrandTheftBombing.Control.GameControl", "class_grand_theft_bombing_1_1_control_1_1_game_control.html", null ]
    ] ],
    [ "GrandTheftBombing.Renderer.GameRenderer", "class_grand_theft_bombing_1_1_renderer_1_1_game_renderer.html", null ],
    [ "GrandTheftBombing.Logic.Graph", "class_grand_theft_bombing_1_1_logic_1_1_graph.html", null ],
    [ "IComponentConnector", null, [
      [ "GrandTheftBombing.Control.ResultWindow", "class_grand_theft_bombing_1_1_control_1_1_result_window.html", null ],
      [ "GrandTheftBombing.Main.MainWindow", "class_grand_theft_bombing_1_1_main_1_1_main_window.html", null ]
    ] ],
    [ "GrandTheftBombing.Model.ICop", "interface_grand_theft_bombing_1_1_model_1_1_i_cop.html", [
      [ "GrandTheftBombing.Model.Cop", "class_grand_theft_bombing_1_1_model_1_1_cop.html", [
        [ "GrandTheftBombing.Model.Player", "class_grand_theft_bombing_1_1_model_1_1_player.html", null ]
      ] ]
    ] ],
    [ "GrandTheftBombing.Model.IDifficultyPreset", "interface_grand_theft_bombing_1_1_model_1_1_i_difficulty_preset.html", [
      [ "GrandTheftBombing.Model.DifficultyPreset", "class_grand_theft_bombing_1_1_model_1_1_difficulty_preset.html", null ]
    ] ],
    [ "GrandTheftBombing.Model.IGameItem", "interface_grand_theft_bombing_1_1_model_1_1_i_game_item.html", null ],
    [ "GrandTheftBombing.Model.IGameModel", "interface_grand_theft_bombing_1_1_model_1_1_i_game_model.html", [
      [ "GrandTheftBombing.Model.GameModel", "class_grand_theft_bombing_1_1_model_1_1_game_model.html", null ]
    ] ],
    [ "GrandTheftBombing.Repository.IHighscoreRecord", "interface_grand_theft_bombing_1_1_repository_1_1_i_highscore_record.html", [
      [ "GrandTheftBombing.Repository.HighscoreRecord", "class_grand_theft_bombing_1_1_repository_1_1_highscore_record.html", null ]
    ] ],
    [ "GrandTheftBombing.Logic.ILogic", "interface_grand_theft_bombing_1_1_logic_1_1_i_logic.html", [
      [ "GrandTheftBombing.Logic.GameLogic", "class_grand_theft_bombing_1_1_logic_1_1_game_logic.html", null ]
    ] ],
    [ "GrandTheftBombing.Model.IMovable", "interface_grand_theft_bombing_1_1_model_1_1_i_movable.html", [
      [ "GrandTheftBombing.Model.Bullet", "class_grand_theft_bombing_1_1_model_1_1_bullet.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "GrandTheftBombing.Repository.IRepository", "interface_grand_theft_bombing_1_1_repository_1_1_i_repository.html", [
      [ "GrandTheftBombing.Repository.GameRepository", "class_grand_theft_bombing_1_1_repository_1_1_game_repository.html", null ]
    ] ],
    [ "List", null, [
      [ "GrandTheftBombing.Control.HSs", "class_grand_theft_bombing_1_1_control_1_1_h_ss.html", null ]
    ] ],
    [ "GrandTheftBombing.Logic.Tests.LogicTester", "class_grand_theft_bombing_1_1_logic_1_1_tests_1_1_logic_tester.html", null ],
    [ "Window", null, [
      [ "GrandTheftBombing.Control.ResultWindow", "class_grand_theft_bombing_1_1_control_1_1_result_window.html", null ],
      [ "GrandTheftBombing.Main.MainWindow", "class_grand_theft_bombing_1_1_main_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "GrandTheftBombing.Control.DifficultyChooser", "class_grand_theft_bombing_1_1_control_1_1_difficulty_chooser.html", null ],
      [ "GrandTheftBombing.Control.GameWindow", "class_grand_theft_bombing_1_1_control_1_1_game_window.html", null ],
      [ "GrandTheftBombing.Control.HighscoreWindow", "class_grand_theft_bombing_1_1_control_1_1_highscore_window.html", null ],
      [ "GrandTheftBombing.Control.MainWindow", "class_grand_theft_bombing_1_1_control_1_1_main_window.html", null ]
    ] ]
];