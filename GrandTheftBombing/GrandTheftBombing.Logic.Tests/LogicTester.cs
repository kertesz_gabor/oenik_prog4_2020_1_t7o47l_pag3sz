// <copyright file="LogicTester.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GrandTheftBombing.Model;
    using GrandTheftBombing.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests the bussiness logic of the program.
    /// </summary>
    [TestFixture]
    public class LogicTester
    {
        /// <summary>
        /// Tests the map loading from resource component.
        /// </summary>
        [Test]
        public void TestMapParsing()
        {
            var modelMock = new Mock<IGameModel>();
            var repoMock = new Mock<IRepository>();
            modelMock.SetupAllProperties();

            ILogic logic = new GameLogic(modelMock.Object, repoMock.Object);
            logic.InitMap("GrandTheftBombing.Logic.Resources.test.lvl");

            Assert.That(() => logic.Model.MyPlayer != null);
            Assert.That(() => logic.Model.MyPlayer.X == 2);
        }

        /// <summary>
        /// Tests the <see cref="ILogic.SetDifficulty(IDifficultyPreset)"/> method for cop generation.
        /// </summary>
        [Test]
        public void TestCopGeneration()
        {
            var modelMock = new Mock<IGameModel>();
            List<Point> entryPoints = new List<Point>() { new Point(0, 1) };
            bool[,] roads = new bool[5, 5];
            roads[0, 2] = true;
            modelMock.SetupAllProperties();

            var difficultyMock = new Mock<IDifficultyPreset>();
            difficultyMock.Setup(t => t.NumberOfCops).Returns(18);
            var repoMock = new Mock<IRepository>();

            GameLogic testLogic = new GameLogic(modelMock.Object, repoMock.Object);
            modelMock.Setup(t => t.MapEntryPoints).Returns(entryPoints);
            modelMock.Setup(t => t.Roads).Returns(roads);

            testLogic.SetDifficulty(difficultyMock.Object);

            Assert.That(modelMock.Object.Cops.Count == 1);  // there's only one entry point
        }

        /// <summary>
        /// Tests whether a cop can identify the direction it should turn in order to catch the player or not.
        /// </summary>
        /// <param name="copX">The x component of the cop's current position.</param>
        /// <param name="copY">The y component of the cop's current position.</param>
        /// <param name="expectedX">The expected x direction component.</param>
        /// <param name="expectedY">The expected y direction component.</param>
        [TestCase(5, 5, -1, 1)]
        [TestCase(-5, 5, 1, 1)]
        [TestCase(-5, -5, 1, -1)]
        [TestCase(5, -5, -1, -1)]
        [TestCase(0, -5, 0, -1)]
        [TestCase(0, 5, 0, 1)]
        [TestCase(5, 0, -1, 0)]
        [TestCase(-5, 0, 1, 0)]
        public void TestDirectionFinder(double copX, double copY, int expectedX, int expectedY)
        {
            // Arrange
            Player myPlayer = new Player() { X = 0, Y = 0 };
            Cop cop = new Cop() { X = copX, Y = copY };
            Point expectedDirection = new Point(expectedX, expectedY);

            Mock<IGameModel> modelMock = new Mock<IGameModel>();
            var repoMock = new Mock<IRepository>();
            modelMock.SetupAllProperties();

            GameLogic logic = new GameLogic(modelMock.Object, repoMock.Object);
            modelMock.Setup(t => t.MyPlayer).Returns(myPlayer);

            // Act
            Point direction = logic.GetDirectionToPlayer(cop);

            // Assert
            Assert.AreEqual(expectedDirection, direction);
        }

        /// <summary>
        /// Tests whether the program can retrieve new directions based on given coordinates.
        /// </summary>
        [Test]
        public void TestGraph()
        {
            Graph graph = new Graph();
            Point p = new Point(1, 1);

            var result = graph.GetPossibleDirections(p);

            Assert.Contains(Direction.Right, result);
            Assert.Contains(Direction.Down, result);
        }

        /// <summary>
        /// Tests the <see cref="ILogic.ChangeVehicleDirection(Cop, Direction)"/> function.
        /// </summary>
        /// <param name="oldDirection">The current direction of a car.</param>
        /// <param name="newDirection">The new direction to be set.</param>
        /// <param name="expectedResult">The expected result.</param>
        [TestCase(Direction.Up, Direction.Left, true)]
        [TestCase(Direction.Left, Direction.Down, true)]
        public void VehicleDirectionChanger(Direction oldDirection, Direction newDirection, bool expectedResult)
        {
            Mock<IGameModel> mockedModel = new Mock<IGameModel>();
            var repoMock = new Mock<IRepository>();
            mockedModel.SetupAllProperties();
            Mock<GameLogic> mockedLogic = new Mock<GameLogic>(mockedModel.Object, repoMock.Object);
            Cop cop = new Cop() { Direction = oldDirection };
            mockedLogic.Setup(t => t.IsValidStep(cop, newDirection)).Returns(true);

            bool actualResult = mockedLogic.Object.ChangeVehicleDirection(cop, newDirection);

            Assert.That(actualResult == expectedResult);
            if (expectedResult)
            {
                Assert.That(cop.Direction == newDirection);
            }
        }

        /// <summary>
        /// Tests if the <see cref="ILogic.IsValidStep(Cop, Direction)"/> method can correctly identify where the player can move next, based on a static test map.
        /// </summary>
        /// <param name="directionToStepTo">The next stepping direction.</param>
        /// <param name="expectedResult">The expected boolean response.</param>
        [TestCase(Direction.Down, false)]
        [TestCase(Direction.Right, false)]
        public void TestIsValidStep(Direction directionToStepTo, bool expectedResult)
        {
            Mock<IGameModel> mockedModel = new Mock<IGameModel>();
            var repoMock = new Mock<IRepository>();
            mockedModel.SetupAllProperties();
            GameLogic logic = new GameLogic(mockedModel.Object, repoMock.Object);
            logic.InitMap("GrandTheftBombing.Logic.Resources.test.lvl");

            bool actualResult = logic.IsValidStep(logic.Model.MyPlayer, directionToStepTo);

            Assert.That(actualResult == expectedResult);
        }
    }
}