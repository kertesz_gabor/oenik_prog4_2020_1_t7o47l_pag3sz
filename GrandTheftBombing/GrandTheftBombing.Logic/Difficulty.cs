﻿// <copyright file="Difficulty.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Logic
{
    using System.Collections.Generic;
    using GrandTheftBombing.Model;

    /// <summary>
    /// Contains the possible difficulties of the game.
    /// </summary>
    public static class Difficulty
    {
        static Difficulty()
        {
            Difficulties = new List<IDifficultyPreset>();
            Difficulties.Add(
                 new DifficultyPreset()
                 {
                     PresetName = "Easy",
                     NumberOfCops = 5,
                 });

            Difficulties.Add(
                 new DifficultyPreset()
                 {
                     PresetName = "Medium",
                     NumberOfCops = 10,
                 });

            Difficulties.Add(
                 new DifficultyPreset()
                 {
                     PresetName = "Hard",
                     NumberOfCops = 15,
                 });
        }

        /// <summary>
        /// Gets or sets the game difficulties.
        /// </summary>
        public static List<IDifficultyPreset> Difficulties { get; set; }
    }
}