// <copyright file="Config.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Logic
{
    /// <summary>
    /// Stores basic configuration parameters for the logic.
    /// </summary>
    public class Config
    {
        /// <summary>
        /// Gets the default speed of the moving vehicles.
        /// </summary>
        public static double SpeedOfVehicles { get; } = 0.1;

        /// <summary>
        /// Gets the in-game tile size measured in pixels.
        /// </summary>
        public static int TileSizeInPixels { get; } = 50;
    }
}