// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using GrandTheftBombing.Model;
    using GrandTheftBombing.Repository;

    /// <summary>
    /// Provides a concrete implementation of the <see cref="ILogic"/> interface.
    /// </summary>
    public class GameLogic : ILogic
    {
        private Graph utilGraph;
        private int remainingCopsToGenerate;
        private Random r = new Random();
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">The <see cref="IGameModel"/> implementation to work with.</param>
        /// <param name="repository">The <see cref="IRepository"/> implementation to work with.</param>
        public GameLogic(IGameModel model, IRepository repository)
        {
            this.model = model;
            this.Repository = repository;
            this.utilGraph = new Graph();
            this.InitMap("GrandTheftBombing.Logic.Resources.mainMap.lvl");
            this.LoadGame();
            this.SetDifficulty(this.GetDifficulties().FirstOrDefault());
        }

        /// <inheritdoc/>
        public IGameModel Model
        {
            get
            {
                return this.model;
            }

            set
            {
                this.model = value;
                this.OnModelChanged();
            }
        }

        /// <inheritdoc/>
        public IRepository Repository { get; set; }

        /// <inheritdoc/>
        public void InitMap(string mapSrc)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(mapSrc);
            string[] lines = null;
            if (stream != null)
            {
                StreamReader sr = new StreamReader(stream);
                lines = sr.ReadToEnd().Replace("\r", string.Empty).Split('\n');
            }
            else
            {
                lines = mapSrc.Split('+').ToArray();
            }

            int width = int.Parse(lines[0]);                // number of tiles
            int height = int.Parse(lines[1]);

            this.Model.Roads = new bool[width, height];  // these are tile-sized
            this.Model.MapEntryPoints = new List<Point>();
            this.Model.Walls = new List<Point>();
            this.Model.Cops = new List<Cop>();

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    char current = lines[y + 2][x];
                    if (current == 'X')
                    {
                        this.Model.Roads[x, y] = true;
                    }
                    else if (current == 'E')
                    {
                        this.Model.Roads[x, y] = true;
                        this.Model.MapEntryPoints.Add(new Point(x, y));
                    }
                    else if (current == 'P')
                    {
                        this.Model.Roads[x, y] = true;
                        this.Model.MyPlayer = new Player()
                        {
                            X = x,
                            Y = y,
                            PlayerPos = new Point(x, y),
                        };
                    }
                    else if (current == 'W')
                    {
                        this.Model.Walls.Add(new Point(x, y));
                    }
                }
            }
        }

        /// <inheritdoc/>
        public bool MovePlayer(Direction direction)
        {
            var isMovable = this.IsPlayerMovable(direction);
            if (isMovable)
            {
                switch (direction)
                {
                    case Direction.Up:
                        this.Model.MyPlayer.Y = Math.Round(this.Model.MyPlayer.Y -= 0.5, 1);
                        this.Model.MyPlayer.Direction = Direction.Up;
                        break;
                    case Direction.Down:
                        this.Model.MyPlayer.Y = Math.Round(this.Model.MyPlayer.Y += 0.5, 1);
                        this.Model.MyPlayer.Direction = Direction.Down;
                        break;
                    case Direction.Right:
                        this.Model.MyPlayer.X = Math.Round(this.Model.MyPlayer.X += 0.5, 1);
                        this.Model.MyPlayer.Direction = Direction.Right;
                        break;
                    case Direction.Left:
                        this.Model.MyPlayer.X = Math.Round(this.Model.MyPlayer.X -= 0.5, 1);
                        this.Model.MyPlayer.Direction = Direction.Left;
                        break;
                }

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool HasThePlayerLost()
        {
            double x = this.Model.MyPlayer.X;
            double y = this.Model.MyPlayer.Y;

            return this.Model.Cops.Any(a => ((a.X < x && a.X + 1 > x) || (a.X < x + 1 && a.X + 1 > x + 1) || (a.X < x + 0.5 && a.X + 1 > x + 0.5))
                                            && ((a.Y < y && a.Y + 1 > y) || (a.Y < y + 1 && a.Y + 1 > y + 1) || (a.Y < y + 0.5 && a.Y + 1 > y + 0.5)));
        }

        /// <inheritdoc/>
        public void MoveCop(Cop cop)
        {
            Point playerRelativePosition = this.GetDirectionToPlayer(cop);
            Queue<Direction> chaseDirections = this.utilGraph.GetPossibleDirections(playerRelativePosition);
            bool successfulStep = false;

            while (!successfulStep)
            {
                try
                {
                    if (cop.StuckCooldown-- <= 0)
                    {
                        successfulStep = this.ChangePosition(cop, chaseDirections.Dequeue());
                        cop.AttemptsToMove++;
                    }
                    else
                    {
                        successfulStep = this.ChangePosition(cop, cop.Direction);
                    }
                }
                catch (Exception)
                {
                    successfulStep = this.ChangePosition(cop, this.GetRandomDirection());
                    cop.AttemptsToMove++;
                    if (successfulStep && cop.AttemptsToMove > 5)
                    {
                        cop.StuckCooldown = 50;
                        cop.AttemptsToMove = 0;
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void Shoot()
        {
            Bullet bullet = null;

            switch (this.Model.MyPlayer.Direction)
            {
                case Direction.Right:
                    bullet = new Bullet(this.Model.MyPlayer.X + 1, this.Model.MyPlayer.Y + 0.5, 0.235, 0);
                    break;
                case Direction.Left:
                    bullet = new Bullet(this.Model.MyPlayer.X, this.Model.MyPlayer.Y + 0.5, -0.235, 0);
                    break;
                case Direction.Down:
                    bullet = new Bullet(this.Model.MyPlayer.X + 0.5, this.Model.MyPlayer.Y + 1, 0, 0.235);
                    break;
                case Direction.Up:
                    bullet = new Bullet(this.Model.MyPlayer.X + 0.5, this.Model.MyPlayer.Y, 0, -0.235);
                    break;
            }

            this.Model.Bullets.Add(bullet);
        }

        /// <inheritdoc/>
        public bool DoesBulletReachWallOrCop(Bullet bullet)
        {
            bullet.Delete = this.DoesObjectGoesIntoWall(Math.Floor(bullet.X), Math.Floor(bullet.Y)) || this.DoesBulletReachCop(Math.Floor(bullet.X), Math.Floor(bullet.Y));
            return bullet.Delete;
        }

        /// <inheritdoc/>
        public void SetDifficulty(IDifficultyPreset chosenPreset)
        {
            int numberOfEntryPoints = this.Model.MapEntryPoints.Count;
            this.Model.Cops = new List<Cop>();
            this.remainingCopsToGenerate = chosenPreset.NumberOfCops;

            foreach (var entryP in this.Model.MapEntryPoints)
            {
                if (this.remainingCopsToGenerate > 0)
                {
                    Cop newCop = new Cop()
                    {
                        X = entryP.X,
                        Y = entryP.Y,
                        Direction = this.GetRandomDirection(),
                    };
                    this.Model.Cops.Add(newCop);
                    this.remainingCopsToGenerate--;
                }
            }
        }

        /// <inheritdoc/>
        public bool ChangeVehicleDirection(Model.Cop vehicle, Direction newDirection)
        {
            if (this.IsValidStep(vehicle, newDirection))
            {
                switch (newDirection)
                {
                    case Direction.Up:
                        vehicle.DX = 0; vehicle.DY = -Config.SpeedOfVehicles;
                        break;
                    case Direction.Right:
                        vehicle.DX = Config.SpeedOfVehicles; vehicle.DY = 0;
                        break;
                    case Direction.Down:
                        vehicle.DX = 0; vehicle.DY = Config.SpeedOfVehicles;
                        break;
                    case Direction.Left:
                        vehicle.DX = -Config.SpeedOfVehicles; vehicle.DY = 0;
                        break;
                }

                vehicle.Direction = newDirection;

                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public void MoveCops()
        {
            foreach (Cop cop in this.Model.Cops)
            {
                this.GetNextStepOfAI(cop);
            }
        }

        /// <inheritdoc/>
        public virtual bool IsValidStep(Model.Cop vehicle, Direction nextDirection)
        {
            double dx = 0;
            double dy = 0;

            if (!((vehicle.Direction == Direction.Down && nextDirection == Direction.Up) ||
                (vehicle.Direction == Direction.Up && nextDirection == Direction.Down) ||
                (vehicle.Direction == Direction.Left && nextDirection == Direction.Right) ||
                (vehicle.Direction == Direction.Right && nextDirection == Direction.Left)))
            {
                switch (nextDirection)
                {
                    case Direction.Up:
                        dx = 0; dy = 1;
                        break;
                    case Direction.Right:
                        dx = 1; dy = 0;
                        break;
                    case Direction.Down:
                        dx = 0; dy = -1;
                        break;
                    case Direction.Left:
                        dx = -1; dy = 0;
                        break;
                }

                int newTileX = Convert.ToInt32((vehicle.X / Config.TileSizeInPixels) + dx);
                int newTileY = Convert.ToInt32((vehicle.Y / Config.TileSizeInPixels) + dy);

                if (!this.DoesObjectGoesIntoWall(newTileX, newTileY) && newTileY > 0 && newTileX > 0 && this.Model.Roads[newTileX, newTileY])
                {
                    return true;
                }

                return false;
            }

            return false;
        }

        /// <inheritdoc/>
        public Point GetDirectionToPlayer(Cop cop)
        {
            double dx = this.Model.MyPlayer.X - cop.X;
            double dy = this.Model.MyPlayer.Y - cop.Y;

            double angle = Math.Atan2(dy, dx);

            if (angle > 0 && angle < Math.PI / 2)
            {
                return new Point(1, -1);
            }
            else if (angle > Math.PI / 2 && angle < Math.PI)
            {
                return new Point(-1, -1);
            }
            else if (angle > -Math.PI && angle < -Math.PI / 2)
            {
                return new Point(-1, 1);
            }
            else if (angle > -Math.PI / 2 && angle < 0)
            {
                return new Point(1, 1);
            }
            else if (dx == 0)
            {
                return new Point(0, -Math.Sign(dy));
            }
            else
            {
                return new Point(Math.Sign(dx), 0);
            }
        }

        /// <inheritdoc/>
        public void SaveGame()
        {
            this.Repository.Save();
        }

        /// <inheritdoc/>
        public List<HighscoreRecord> LoadGame()
        {
            this.Repository.Load();
            return this.Repository.Highscores;
        }

        /// <inheritdoc/>
        public List<IDifficultyPreset> GetDifficulties()
        {
            return Difficulty.Difficulties;
        }

        private void GetNextStepOfAI(Cop cop)
        {
            Point playerRelativePosition = this.GetDirectionToPlayer(cop);
            Queue<Direction> chaseDirections = this.utilGraph.GetPossibleDirections(playerRelativePosition);
            bool successfulStep = false;

            do
            {
                try
                {
                    successfulStep = this.ChangeVehicleDirection(cop, chaseDirections.Dequeue());
                }
                catch (InvalidOperationException)
                {
                    do
                    {
                        successfulStep = this.ChangeVehicleDirection(cop, this.GetRandomDirection());
                    }
                    while (!successfulStep);
                }
            }
            while (!successfulStep);
        }

        private Direction GetRandomDirection()
        {
            Array values = Enum.GetValues(typeof(Direction));
            return (Direction)values.GetValue(this.r.Next(values.Length));
        }

        private void OnModelChanged()
        {
            this.remainingCopsToGenerate = this.Model.SelectedDifficulty.NumberOfCops - this.Model.Cops.Count;
        }

        private bool IsPlayerMovable(Direction direction)
        {
            double x = this.Model.MyPlayer.X;
            double y = this.Model.MyPlayer.Y;
            var size = this.Model.TileSize;

            switch (direction)
            {
                case Direction.Up:
                    y -= 0.1;
                    break;
                case Direction.Down:
                    y += 0.1;
                    break;
                case Direction.Right:
                    x += 0.1;
                    break;
                case Direction.Left:
                    x -= 0.1;
                    break;
            }

            if (x < 0 || y < 0 || this.Model.GameHeight - 1 < y || this.Model.GameWidth - 1 < x)
            {
                return false;
            }

            return !this.DoesObjectGoesIntoWall(x, y);
        }

        private bool ChangePosition(Cop cop, Direction nextDirection)
        {
            double x = cop.X;
            double y = cop.Y;
            if (nextDirection == Direction.Up)
            {
                y = Math.Round(y + 0.1, 1);
            }
            else if (nextDirection == Direction.Down)
            {
                y = Math.Round(y - 0.1, 1);
            }
            else if (nextDirection == Direction.Left)
            {
                x = Math.Round(x - 0.1, 1);
            }
            else
            {
                x = Math.Round(x + 0.1, 1);
            }

            if (!this.DoesObjectGoesIntoWall(x, y) && !this.CopDoesGoIntoOtherCop(cop, x, y))
            {
                cop.X = x;
                cop.Y = y;
                cop.Direction = nextDirection;
                return true;
            }

            return false;
        }

        private bool DoesBulletReachCop(double x, double y)
        {
            var damagedCop = this.DamagedCop(x, y);

            if (damagedCop == null)
            {
                return false;
            }

            this.Model.Cops.Remove(damagedCop);
            this.Model.MyPlayer.Kills++;

            if (this.remainingCopsToGenerate > 0)
            {
                var newStartPos = this.Model.MapEntryPoints[this.r.Next(this.Model.MapEntryPoints.Count - 1)];
                Cop newCop = new Cop()
                {
                    X = newStartPos.X,
                    Y = newStartPos.Y,
                    Direction = this.GetRandomDirection(),
                };
                this.Model.Cops.Add(newCop);
                this.remainingCopsToGenerate--;
            }

            return true;
        }

        private bool DoesObjectGoesIntoWall(double x, double y)
        {
            return this.Model.Walls.Any(a => ((a.X < x && a.X + 1 > x) || (a.X < x + 1 && a.X + 1 > x + 1) || (a.X < x + 0.5 && a.X + 1 > x + 0.5)) &&
                                            ((a.Y < y && a.Y + 1 > y) || (a.Y < y + 1 && a.Y + 1 > y + 1) || (a.Y < y + 0.5 && a.Y + 1 > y + 0.5)));
        }

        private Cop DamagedCop(double x, double y)
        {
            return this.Model.Cops.FirstOrDefault(a => ((a.X < x && a.X + 1 > x) || (a.X < x + 1 && a.X + 1 > x + 1) || (a.X < x + 0.5 && a.X + 1 > x + 0.5)) &&
                                                        ((a.Y < y && a.Y + 1 > y) || (a.Y < y + 1 && a.Y + 1 > y + 1) || (a.Y < y + 0.5 && a.Y + 1 > y + 0.5)));
        }

        private bool CopDoesGoIntoOtherCop(Cop c, double x, double y)
        {
            return this.Model.Cops.Any(a => a != c
                                            && ((a.X < x && a.X + 1 > x) || (a.X < x + 1 && a.X + 1 > x + 1) || (a.X < x + 0.5 && a.X + 1 > x + 0.5))
                                            && ((a.Y < y && a.Y + 1 > y) || (a.Y < y + 1 && a.Y + 1 > y + 1) || (a.Y < y + 0.5 && a.Y + 1 > y + 0.5)));
        }
    }
}
