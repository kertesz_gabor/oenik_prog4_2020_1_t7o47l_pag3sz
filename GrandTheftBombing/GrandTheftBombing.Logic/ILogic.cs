﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Logic
{
    using System.Collections.Generic;
    using System.Drawing;
    using GrandTheftBombing.Model;
    using GrandTheftBombing.Repository;

    /// <summary>
    /// Defines the components needed for the game's logic.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Gets or sets an <see cref="IGameModel"/> implementation.
        /// </summary>
        IGameModel Model { get; set; }

        /// <summary>
        /// Gets or sets an <see cref="IRepository"/> implementation.
        /// </summary>
        IRepository Repository { get; set; }

        /// <summary>
        /// Sets the difficulty of the game by generating cops.
        /// </summary>
        /// <param name="chosenPreset">The preset to set cop number according to.</param>
        void SetDifficulty(IDifficultyPreset chosenPreset);

        /// <summary>
        /// Given that the new direction is a possible step, changes the given vehicle's direction to it.
        /// </summary>
        /// <param name="vehicle">The vehicle to change.</param>
        /// <param name="newDirection">The new direction the vehicle should be facing into.</param>
        /// <returns>True or false, based on whether the change was successful or not.</returns>
        bool ChangeVehicleDirection(Cop vehicle, Direction newDirection);

        /// <summary>
        /// Gets the point where the given cop should try to move into in the next step.
        /// </summary>
        /// <param name="cop">The cop that should be moved.</param>
        /// <returns>The demanded point.</returns>
        Point GetDirectionToPlayer(Cop cop);

        /// <summary>
        /// Initializes the current <see cref="IGameModel"/> implementation according to the given resource.
        /// </summary>
        /// <param name="mapSrc">The resource representing the gamemap found in the executing assembly.</param>
        void InitMap(string mapSrc);

        /// <summary>
        /// Decides whether a vehicle can turn into a given direction or not.
        /// </summary>
        /// <param name="vehicle">The vehicle we would like to turn.</param>
        /// <param name="nextDirection">The wanted next direction.</param>
        /// <returns>A boolean value representing the validation of the step.</returns>
        bool IsValidStep(Cop vehicle, Direction nextDirection);

        /// <summary>
        /// Moves all the cops in the current <see cref="IGameModel"/>.
        /// </summary>
        void MoveCops();

        /// <summary>
        /// Saves the current state of the <see cref="IGameModel"/> component by the help of the set <see cref="IRepository"/>.
        /// </summary>
        void SaveGame();

        /// <summary>
        /// Loads the highscore records.
        /// </summary>
        /// <returns>A list of the highscores.</returns>
        List<HighscoreRecord> LoadGame();

        /// <summary>
        /// Determines whether a bullet reaches a wall or a cop.
        /// </summary>
        /// <param name="bullet">The analyzed bullet.</param>
        /// <returns>True or false.</returns>
        bool DoesBulletReachWallOrCop(Bullet bullet);

        /// <summary>
        /// Determines whether the player has lost or not.
        /// </summary>
        /// <returns>True or false.</returns>
        bool HasThePlayerLost();

        /// <summary>
        /// Moves the player on the game map.
        /// </summary>
        /// <param name="direction">The direction to move the player in.</param>
        /// <returns>The result of the moving.</returns>
        bool MovePlayer(Direction direction);

        /// <summary>
        /// Provides logic for player shooting.
        /// </summary>
        void Shoot();

        /// <summary>
        /// Moves a cop instance.
        /// </summary>
        /// <param name="cop">The cop to move.</param>
        void MoveCop(Cop cop);

        /// <summary>
        /// Gets the list of difficulty presets.
        /// </summary>
        /// <returns>A list of <see cref="IDifficultyPreset"/>s.</returns>
        List<IDifficultyPreset> GetDifficulties();
    }
}
