// <copyright file="Graph.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Logic
{
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using GrandTheftBombing.Model;

    /// <summary>
    /// Provides a storage solutions for connecting directions to direction coordinates.
    /// </summary>
    public class Graph
    {
        /// <summary>
        /// Provides a matrix interface for storing graph compatibility.
        /// </summary>
        private bool[,] compatibleNodes;

        /// <summary>
        /// Initializes a new instance of the <see cref="Graph"/> class. Creates the node compatibilities.
        /// </summary>
        public Graph()
        {
            this.CreateNodes();
        }

        /// <summary>
        /// Gets or sets the nodes in the graph.
        /// </summary>
        public List<object> Nodes { get; set; }

        /// <summary>
        /// Gets the directions we can move next, in order to move towards a given point.
        /// </summary>
        /// <param name="p">The adjacent point we want to step toward.</param>
        /// <returns>A <see cref="Queue{Direction}"/> containing the possible next moving directions.</returns>
        public Queue<Direction> GetPossibleDirections(Point p)
        {
            Queue<Direction> possibleDirections = new Queue<Direction>();
            int nodeNumber = this.Nodes.IndexOf(p);
            for (int i = 8; i < this.compatibleNodes.GetLength(1); i++)
            {
                if (this.compatibleNodes[nodeNumber, i])
                {
                    possibleDirections.Enqueue((Direction)this.Nodes.ElementAt(i));
                }
            }

            return possibleDirections;
        }

        private void CreateNodes()
        {
            this.Nodes = new List<object>();
            this.compatibleNodes = new bool[8, 12];
            this.Nodes.Add(new Point(1, -1));   // index 0
            this.Nodes.Add(new Point(-1, -1));
            this.Nodes.Add(new Point(-1, 1));
            this.Nodes.Add(new Point(1, 1));
            this.Nodes.Add(new Point(0, 1));
            this.Nodes.Add(new Point(0, -1));
            this.Nodes.Add(new Point(1, 0));
            this.Nodes.Add(new Point(-1, 0));   // index 7

            this.Nodes.Add(Direction.Down);     // index 8
            this.Nodes.Add(Direction.Left);     // index 9
            this.Nodes.Add(Direction.Right);    // index 10
            this.Nodes.Add(Direction.Up);       // index 11

            for (int i = 0; i < 7; i++)
            {
                if (((Point)this.Nodes[i]).X == 1)
                {
                    this.compatibleNodes[i, 10] = true;
                }

                if (((Point)this.Nodes[i]).X == -1)
                {
                    this.compatibleNodes[i, 9] = true;
                }

                if (((Point)this.Nodes[i]).Y == -1)
                {
                    this.compatibleNodes[i, 11] = true;
                }

                List<object> nodes = this.Nodes;
                if (((Point)nodes[i]).Y == 1)
                {
                    this.compatibleNodes[i, 8] = true;
                }
            }
        }
    }
}