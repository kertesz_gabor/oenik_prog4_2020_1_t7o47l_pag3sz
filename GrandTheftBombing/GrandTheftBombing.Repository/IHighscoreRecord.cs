﻿// <copyright file="IHighscoreRecord.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Repository
{
    using System;

    /// <summary>
    /// Provides a structure for how a highscore record should look like.
    /// </summary>
    public interface IHighscoreRecord
    {
        /// <summary>
        /// Gets or sets the user the game was played by.
        /// </summary>
        string User { get; set; }

        /// <summary>
        /// Gets or sets the number of cops killed by the user in the game.
        /// </summary>
        int CopsEliminated { get; set; }

        /// <summary>
        /// Gets or sets the date the game was played on.
        /// </summary>
        DateTime Date { get; set; }
    }
}
