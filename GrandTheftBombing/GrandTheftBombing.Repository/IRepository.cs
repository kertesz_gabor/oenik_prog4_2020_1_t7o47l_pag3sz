﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Repository
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides functionality for storing a HS record some way. Could be implemented by some XML handler or a database solution.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Gets or sets the list of highscore records.
        /// </summary>
        List<HighscoreRecord> Highscores { get; set; }

        /// <summary>
        /// Saves the game's state.
        /// </summary>
        void Save();

        /// <summary>
        /// Loads the highscore records.
        /// </summary>
        void Load();
    }
}