﻿// <copyright file="GameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using GrandTheftBombing.Model;

    /// <summary>
    /// Provides an <see cref="IRepository"/> implementation.
    /// </summary>
    public class GameRepository : IRepository
    {
        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRepository"/> class.
        /// </summary>
        /// <param name="model">The <see cref="IGameModel"/> implementation to work with.</param>
        public GameRepository(IGameModel model)
        {
            this.model = model;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRepository"/> class.
        /// </summary>
        public GameRepository()
        {
            this.Load();
        }

        /// <inheritdoc/>
        public List<HighscoreRecord> Highscores { get; set; }

        /// <inheritdoc/>
        public void Load()
        {
            this.Highscores = new List<HighscoreRecord>();
            string user, kills, date = string.Empty;
            try
            {
                XDocument xdoc = XDocument.Load("highscores.xml");
                foreach (XElement item in xdoc.Descendants("highscore"))
                {
                    user = item.Attribute("user")?.Value;
                    kills = item.Attribute("kills").Value;
                    date = item.Attribute("date").Value;
                    this.Highscores.Add(new HighscoreRecord(user, int.Parse(kills), Convert.ToDateTime(date)));
                }
            }
            catch
            {
            }

            List<HighscoreRecord> hs = new List<HighscoreRecord>(this.Highscores.OrderByDescending(x => x.CopsEliminated));
            this.Highscores.Clear();
            this.Highscores.AddRange(hs);
        }

        /// <inheritdoc/>
        public void Save()
        {
            if (this.Highscores.Count > 5)
            {
                if (this.model.MyPlayer.Kills > this.Highscores.Min(x => x.CopsEliminated))
                {
                    this.Highscores = this.Highscores.OrderBy(x => x.CopsEliminated).ToList();
                    var behindWho = this.Highscores.Find(x => x.CopsEliminated < this.model.MyPlayer.Kills);
                    var swapIdx = this.Highscores.FindIndex(x => x.Equals(behindWho));
                    this.Highscores.RemoveAt(swapIdx);
                    this.Highscores.Insert(swapIdx, new HighscoreRecord(
                                                    this.model.MyPlayer.Name,
                                                    this.model.MyPlayer.Kills,
                                                    DateTime.Now));
                }
            }
            else
            {
                this.Highscores.Add(new HighscoreRecord(
                                                this.model.MyPlayer.Name,
                                                this.model.MyPlayer.Kills,
                                                DateTime.Now));
            }

            this.Export();
        }

        private void Export()
        {
            File.Delete("highscores.xml");
            XmlWriter xmlWriter = XmlWriter.Create("highscores.xml");
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("highscores");

            foreach (var item in this.Highscores)
            {
                xmlWriter.WriteStartElement("highscore");
                if (item.User == null)
                {
                    item.User = "Anonymus";
                }

                xmlWriter.WriteAttributeString("user", item.User);
                xmlWriter.WriteAttributeString("kills", item.CopsEliminated.ToString());
                xmlWriter.WriteAttributeString("date", item.Date.ToString());
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }
    }
}
