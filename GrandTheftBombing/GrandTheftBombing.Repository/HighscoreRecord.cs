﻿// <copyright file="HighscoreRecord.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GrandTheftBombing.Repository
{
    using System;

    /// <summary>
    /// An <see cref="IHighscoreRecord"/> implementation.
    /// </summary>
    public class HighscoreRecord : IHighscoreRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreRecord"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="copseliminated">Number of cops killed.</param>
        /// <param name="date">The gameplay's date.</param>
        public HighscoreRecord(string user, int copseliminated, DateTime date)
        {
            this.User = user;
            this.CopsEliminated = copseliminated;
            this.Date = date;
        }

        /// <inheritdoc/>
        public string User { get; set; }

        /// <inheritdoc/>
        public int CopsEliminated { get; set; }

        /// <inheritdoc/>
        public DateTime Date { get; set; }
    }
}
