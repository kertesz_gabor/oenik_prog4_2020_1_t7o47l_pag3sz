# Interface list

     Grand Theft Bombing
     OE-NIK 2020 - SZTGUI
     Márki Hajnalka - PAG3SZ
     Kertész Gábor – T7O47L

## Model

### IPlayer

- Életek száma
- Irány
- Pozíció (X,Y)
- Sebesség(dX,dY)

### ICop

- Irány
- Pozíció (X,Y)
- Sebesség (dX,dY)

### IGameItem

> Items like shells, bombs, etc  

- Pozíció (X,Y)
- SetX()
- SetY()
- SetPosition(X,Y)

### IDifficultyPreset

- Zsaruk száma
- Játékos életeinek száma

## Repository

### IHsRecord

- Név
- Nehézségi preset
- Dátum

### IGtbRepositoy

- Save()
- Load()
- HsRecord gyűjtemény

## Logic

- Model 
- Repo